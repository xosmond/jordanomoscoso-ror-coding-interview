# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user

  validates :body, length: { maximum: 180 }
  validate :validate_body_from_same_user_in_24_hours_from_creation_time

  scope :from_non_company_users, -> { joins(:user).where(users: { company_id: nil }) }
  scope :from_company_users, -> { joins(:user).where.not(users: { company_id: nil }) }

  def validate_body_from_same_user_in_24_hours_from_creation_time
    if Tweet.where('created_at > ?', 24.hours.ago).exists?(user_id: user_id, body: body)
      errors.add :body, 'Same tweet body have been created in less or 24 hours ago'
    end
  end
end
