require 'rails_helper'

RSpec.describe Tweet, type: :model do
  describe 'validations' do
    describe 'body validations' do
      describe 'when body is greater than 180 length' do
        let(:tweet) { build(:tweet, body: 'a' * 181) }
  
        it 'is NOT valid' do
          expect(tweet.valid?).to be(false)
        end
      end
  
      describe 'when body is less or equal 100 length' do
        let(:tweet) { build(:tweet, body: 'a' * 180) }
  
        it 'is valid' do
          expect(tweet.valid?).to be(true)
        end
      end
    end

    describe 'custom validations' do
      describe 'when same tweet (body and user) is submitted within 24 hrs' do
        let(:body) { 'same' }
        let(:user) { create(:user) }
        let(:tweet) { build(:tweet, user: user, body: body) }

        before do
          create(:tweet, user: user, body: body)
        end

        it 'is NOT valid' do
          expect(tweet.valid?).to be(false)
        end
      end

      describe 'when same tweet (body and user) is submitted after 24 hrs' do
        let(:body) { 'same' }
        let(:user) { create(:user) }
        let(:tweet) { build(:tweet, user: user, body: body) }

        before do
          create(:tweet, user: user, body: body, created_at: 25.hours.ago)
        end

        it 'is valid' do
          expect(tweet.valid?).to be(true)
        end
      end
    end    
  end
end
